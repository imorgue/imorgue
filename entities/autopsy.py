class Autopsy:

	def __init__(self, pathomorphologist, patient, date, micro, macro, pat_diagnosis):
		self.pathomorphologist = pathomorphologist
		self.patient = patient
		self.date = date
		self.micro = micro
		self.macro = macro
		self.pat_diagnosis = pat_diagnosis

	def __str__(self):
		return "%s\t%s\t%s\t%s\t%s\t%s\n" % (self.pathomorphologist, self.patient, self.date, self.micro, self.macro, self.pat_diagnosis)
