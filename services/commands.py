
from entities.pathomorphologist import Pathomorphologist
from entities.autopsy import Autopsy
import datetime

class Commands:
	
	def addToTheBase(self, pathomorphologist):
		patients_file = open("C:/Users/sviperm/Documents/iMorgue/repositories/list_of_patients.txt", "r") 
		pfile = patients_file.readlines()
		patients_file.close() 
		pasport = raw_input("Enter passport id of patient: ") 
		n = 0
		while n <= len(pfile):
			line = pfile[n]
			lst = line.split("\t") 
			if pasport == lst[0]:		
				dt = datetime.datetime.today()	
				corpses_file = open("C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt", "a")
				corpses_file.write(str(Autopsy(pathomorphologist = pathomorphologist.login, \
											   patient = line.strip(), \
											   date = "%s.%s.%s %s:%s" % (dt.day, dt.month, dt.year, dt.hour, dt.minute), \
											   micro = raw_input("\tEnter micro: "), \
											   macro = raw_input("\tEnter macro: "), \
											   pat_diagnosis = raw_input("\tEnter pat. diagnosis: "))))
				print "Corpse was added"
				corpses_file.close()
				n = len(pfile) + 1
			elif pasport != lst[0] and len(pfile) >= n:
				if len(pfile) > n:
					n += 1
				if len(pfile) == n:
					print "Not Found"
					break
	
	def delFromTheBase(self, pathomorphologist):
		passport_id = raw_input("Type pasport id of coprose: ")
		old_list_of_corpses = open('C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt', 'r')
		file = old_list_of_corpses.readlines()
		old_list_of_corpses.close()
		n = 0	
		while n <= len(file):
			line = file[n]
			lst = line.split("\t")
			if passport_id == lst[1] and pathomorphologist.login == lst[0]:
				del file[n]
				new_list_of_corpses = open('C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt', 'w')
				new_list_of_corpses.writelines(file)
				new_list_of_corpses.close()
				print "Coprose was deleted!"
				n = len(file) + 1
			elif passport_id == lst[1] and pathomorphologist.login != lst[0]:
				print "You can't delete this coprose, you don't have permission"
				n = len(file) + 1
			if passport_id != lst[1]and len(file) >= n:
				if len(file) > n:
					n += 1
				if len(file) == n:
					print "Not Found"
					break
	
	def editCoprose(self, pathomorphologist):
		passport_id = raw_input("Type pasport id of coprose: ")
		old_list_of_corpses = open('C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt', 'r')
		file = old_list_of_corpses.readlines()
		old_list_of_corpses.close()
		n = 0	
		while n <= len(file):
			line = file[n]
			lst = line.split("\t")
			if passport_id == lst[1] and pathomorphologist.login == lst[0]:
				file[n] = str(Autopsy(pathomorphologist = lst[0], \
									  patient = lst[1] + "\t" + lst[2] + "\t" +lst[3] + "\t" +lst[4] + "\t" +lst[5] + "\t" +lst[6] + "\t" +lst[7] + "\t", \
									  date = lst[8], \
									  micro = raw_input("\tEnter micro: "), \
									  macro = raw_input("\tEnter macro: "), \
									  pat_diagnosis = raw_input("\tEnter pat. diagnosis: ")))
				new_list_of_corpses = open('C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt', 'w')
				new_list_of_corpses.writelines(file)
				new_list_of_corpses.close()
				print "Coprose was edit!"
				n = len(file) + 1
			elif passport_id == lst[1] and pathomorphologist.login != lst[0]:
				print "You can't edit this coprose, you don't have permission!"
				n = len(file) + 1
			if passport_id != lst[1]and len(file) >= n:
				if len(file) > n:
					n += 1
				if len(file) == n:
					print "Not Found"
					break
	
	def showStatistics(self):
		print "Not enough information Q__Q"
	
	def showWhoAmI(self, whoAmI):
		print "24601?.. No. You are %s!" % whoAmI.fio

	def showMeTheList(self):
		command = raw_input("List of patinet or list of corpses? (p/c) : ")
		print ""
		if command == "p":
			file = open("C:/Users/sviperm/Documents/iMorgue/repositories/list_of_patients.txt", "r")
			for line in file:
				print line
			file.close()
		elif command == "c":
			file = open("C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt", "r")
			for line in file:
				print line
			file.close()

	def searchInTheBase(self):
		search = raw_input("Type passport id or FIO: ")
		file = open("C:/Users/sviperm/Documents/iMorgue/repositories/list_of_corpses.txt", "r")
		check = False
		first_line = True
		for line in file:
			lst = line.split("\t")
			word = lst[1] + " " + lst[2]
			if search in word:
				if first_line:
					print "\n"
					first_line = False
				print line
				check = True
		if not check:
			print "Not found"
		file.close()