from entities.pathomorphologist import Pathomorphologist
from services.commands import Commands

class MainMenu:	

	def showMainMenu(self, pathomorphologist):	
			print "=====================MAIN MENU=====================\nFor help type 'help'."
			command = Commands()
			restart = True
			while restart:
				commandStr = raw_input("\nType command: ")
				if commandStr == "help":
					print "add - add to the base\ndel - delete from the base\nedit - edit the base\nlst - show the list of corposes\nsrch - search corpse in the base\nstat - show statistics\nwho - if u want to know who you are\nlo - log out"
				elif commandStr == "add":
					command.addToTheBase(pathomorphologist)
				elif commandStr == "del":
					command.delFromTheBase(pathomorphologist)
				elif commandStr == "edit":
					command.editCoprose(pathomorphologist)
				elif commandStr == "lst":
					command.showMeTheList()
				elif commandStr == "srch":
					command.searchInTheBase()
				elif commandStr == "stat":
					command.showStatistics()
				elif commandStr == "who":
					command.showWhoAmI(pathomorphologist)
				elif commandStr == "search":
					command.searchInTheBase()
				elif commandStr == "lo":
					commandStr = raw_input("Are you sure you want to log out? (y/n): ")
					if commandStr == "y":
						print ""
						restart = False
					else:
						pass
				else:
					print "Unknown command, try again"